svmInputPos = zeros([9, 10000]);
groupsPos = ones([9, 1]);
for i = 1:9
    train_image = im2double(imread(strcat('/home/matthew/Downloads/Data/pos/pos_',num2str(i, '%04d'),'.png')));
    boundaries = csvread(strcat('/home/matthew/Downloads/Data/pos/pos_',num2str(i, '%04d'),'.txt'));
    train_image = train_image(boundaries(2):boundaries(4), boundaries(1):boundaries(3),:);
    train_image = HSVTransform(train_image);
    [gradMag, gradDir] = ComputeGradients(train_image(:,:,3), [-1 0 1]);
    binned = ComputeHistogram(gradDir, [5 5], [-180 180], 18);
    featureVector = SubSample(ContrastNormalize(binned, [5 5], [3 3]), 1, 10000);
    svmInputPos(i, :) = featureVector;
    i
end

svmInputNeg = zeros([9, 10000]); 
groupsNeg = zeros([9 1]);
for i = 1:9
    train_image = im2double(imread(strcat('/home/matthew/Downloads/Data/neg/neg_',num2str(i, '%04d'),'.png')));
    boundaries = csvread(strcat('/home/matthew/Downloads/Data/neg/neg_',num2str(i, '%04d'),'.txt'));
    train_image = train_image(boundaries(2):boundaries(4), boundaries(1):boundaries(3),:);
    train_image = HSVTransform(train_image);
    [gradMag, gradDir] = ComputeGradients(train_image(:,:,3), [-1 0 1]);
    binned = ComputeHistogram(gradDir, [5 5], [-180 180], 18);
    featureVector = SubSample(ContrastNormalize(binned, [5 5], [3 3]), 1, 10000);
    svmInputNeg(i, :) = featureVector;
    i
end
svmInput = vertcat(svmInputPos, svmInputNeg);
groups = vertcat(groupsPos,groupsNeg);
SVMStruct = svmtrain(svmInput, groups, 'showPlot', true);

train_image = im2double(imread(strcat('/home/matthew/Downloads/Data/neg/neg_0012.png')));
boundaries = csvread(strcat('/home/matthew/Downloads/Data/neg/neg_0012.txt'));
train_image = train_image(boundaries(2):boundaries(4), boundaries(1):boundaries(3),:);
train_image = HSVTransform(train_image);
[gradMag, gradDir] = ComputeGradients(train_image(:,:,3), [-1 0 1]);
binned = ComputeHistogram(gradDir, [5 5], [-180 180], 18);
featureVector = SubSample(ContrastNormalize(binned, [5 5], [3 3]), 1, 10000);
svmclassify(SVMStruct, featureVector);